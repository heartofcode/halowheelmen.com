module.exports = {
  siteMetadata: {
    title: `Halowheelmen`,
    description: `Founded in 2008, the Wheelmen are a Halo clan for teamwork-oriented players and fans of the Warthog.`,
    author: `@heartofcode`,
  },
  plugins: [
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}
