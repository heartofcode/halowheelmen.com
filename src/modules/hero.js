
import React from 'react'

import { useWindowSize } from 'components/hooks/useWindowSize'

export const Hero = (props) => {

    let windowSize = useWindowSize()
    let { image, video, videoType = 'video/mp4' } = props

    console.log(windowSize)

    return(

        <>

            {windowSize.width >= 600 &&
                <img src={image} alt='' />
            }

            {windowSize.width < 600 &&
                <video width='auto' height='auto' autoplay loop muted>
                    <source src={video} type={videoType} />
                </video>
            }

        </>

    )

}